package com.learn.kafka.client.service.impl;

import com.learn.kafka.client.repository.OrderRepository;
import com.learn.kafka.client.service.ReceiverService;
import com.learn.kafka.entity.Order;
import com.learn.kafka.entity.Receiver;
import com.learn.kafka.entity.dto.OrderDTO;
import com.learn.kafka.entity.dto.ReceiverDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.kafka.core.KafkaTemplate;

import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OrderServiceImplTest {

    @InjectMocks
    private OrderServiceImpl orderService;

    @Mock
    private OrderRepository orderRepositoryMock;

    @Mock
    private ReceiverService receiverServiceMock;

    @Mock
    private KafkaTemplate<Long, OrderDTO> kafkaTemplateMock;

    @Mock
    private Order orderMock;

    @Mock
    private Receiver receiverMock;

    private final long orderId = 1L;

    @Test
    public void getOrderInformationTest() {

        when(orderRepositoryMock.findById(orderId)).thenReturn(Optional.of(orderMock));

        orderService.getOrderInformation(orderId);

        verify(orderRepositoryMock).findById(orderId);
    }

    @Test
    public void saveTest() {
        String orderTopicName = "order";
        long userId = 1L;
        when(receiverServiceMock.findById(userId)).thenReturn(receiverMock);
        when(orderMock.getId()).thenReturn(orderId);
        when(orderRepositoryMock.save(orderMock)).thenReturn(orderMock);

        orderService.save(orderMock, userId);

        verify(orderRepositoryMock).save(orderMock);
        verify(kafkaTemplateMock).send(
                orderTopicName, orderId, new OrderDTO(orderMock.getName(), new ReceiverDTO(), orderMock.getOrderStatus())
        );
    }
}