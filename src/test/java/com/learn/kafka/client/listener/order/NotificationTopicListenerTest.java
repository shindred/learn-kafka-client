package com.learn.kafka.client.listener.order;

import com.learn.kafka.client.repository.OrderRepository;
import com.learn.kafka.client.service.OrderService;
import com.learn.kafka.entity.Order;
import com.learn.kafka.entity.dto.OrderDTO;
import com.learn.kafka.entity.dto.ReceiverDTO;
import com.learn.kafka.entity.enumaration.OrderStatus;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class NotificationTopicListenerTest {

    @InjectMocks
    private NotificationTopicListener notificationTopicListener;

    @Mock
    private OrderRepository orderRepositoryMock;

    @Mock
    private OrderService orderServiceMock;

    @Mock
    private Order orderMock;

    @Test
    public void listenTest() {
        long orderId = 1L;
        OrderStatus orderStatus = OrderStatus.READY_FOR_DELIVERY;
        OrderDTO orderDTO = new OrderDTO("", new ReceiverDTO(), orderStatus);
        when(orderServiceMock.getOrderInformation(orderId)).thenReturn(orderMock);

        notificationTopicListener.listen(orderDTO, orderId);

        verify(orderMock).setOrderStatus(orderStatus);
        verify(orderRepositoryMock).save(orderMock);
    }
}