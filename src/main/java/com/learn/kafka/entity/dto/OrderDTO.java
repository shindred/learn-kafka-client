package com.learn.kafka.entity.dto;

import com.learn.kafka.entity.enumaration.OrderStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderDTO {

    private String name;
    private ReceiverDTO receiverDTO;
    private OrderStatus orderStatus;
}
