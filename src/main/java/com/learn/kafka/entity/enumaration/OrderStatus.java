package com.learn.kafka.entity.enumaration;

public enum OrderStatus {
    PREPARING, READY_FOR_DELIVERY, DELIVERING, DELIVERED;
}
