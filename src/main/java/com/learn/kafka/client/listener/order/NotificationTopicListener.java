package com.learn.kafka.client.listener.order;

import com.learn.kafka.client.repository.OrderRepository;
import com.learn.kafka.client.service.OrderService;
import com.learn.kafka.entity.Order;
import com.learn.kafka.entity.dto.OrderDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.TopicPartition;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class NotificationTopicListener {

    private final OrderService orderService;
    private final OrderRepository orderRepository;

    @KafkaListener(topicPartitions = @TopicPartition(topic = "notification", partitions = {"0"}))
    public void listen(@Payload OrderDTO orderDTO, @Header(KafkaHeaders.RECEIVED_MESSAGE_KEY) Long id) {
        Order orderFromDB = orderService.getOrderInformation(id);
        orderFromDB.setOrderStatus(orderDTO.getOrderStatus());
        orderRepository.save(orderFromDB);
    }
}
