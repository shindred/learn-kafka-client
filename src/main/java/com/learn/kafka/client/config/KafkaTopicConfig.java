package com.learn.kafka.client.config;

import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.KafkaAdmin;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class KafkaTopicConfig {

    @Value("${spring.kafka.bootstrap-servers}")
    private String kafkaServer;

    @Bean
    public KafkaAdmin kafkaAdmin() {
        Map<String, Object> properties = new HashMap<>();

        properties.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaServer);

        return new KafkaAdmin(properties);
    }

    @Bean
    public NewTopic topic1() {
        return new NewTopic("order", 1, (short) 1);
    }

    @Bean
    public NewTopic topic2() {
        return new NewTopic("notification", 2, (short) 1);
    }
}
