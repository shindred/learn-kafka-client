package com.learn.kafka.client.service;

import com.learn.kafka.entity.Order;
import com.learn.kafka.entity.dto.OrderDTO;

public interface OrderService {

    Order getOrderInformation(Long id);

    OrderDTO save(Order order, Long userId);
}
