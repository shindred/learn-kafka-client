package com.learn.kafka.client.service.impl;

import com.learn.kafka.entity.Order;
import com.learn.kafka.entity.Receiver;
import com.learn.kafka.entity.dto.OrderDTO;
import com.learn.kafka.entity.dto.ReceiverDTO;
import com.learn.kafka.entity.enumaration.OrderStatus;
import com.learn.kafka.client.repository.OrderRepository;
import com.learn.kafka.client.service.OrderService;
import com.learn.kafka.client.service.ReceiverService;
import lombok.RequiredArgsConstructor;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    private static final String ORDER_TOPIC_NAME = "order";

    private final OrderRepository orderRepository;
    private final ReceiverService receiverService;
    private final KafkaTemplate<Long, OrderDTO> orderDTOKafkaTemplate;

    @Override
    public Order getOrderInformation(Long id) {
        return orderRepository.findById(id).orElseThrow(RuntimeException::new);
    }

    @Override
    public OrderDTO save(Order order, Long userId) {
        Receiver receiver = receiverService.findById(userId);
        order.setReceiver(receiver);
        order.setOrderStatus(OrderStatus.PREPARING);
        Order savedOrder = orderRepository.save(order);

        ReceiverDTO receiverDTO = new ReceiverDTO(receiver.getFirstName(), receiver.getSecondName(), receiver.getAddress());
        OrderDTO orderDTO = new OrderDTO(savedOrder.getName(), receiverDTO, savedOrder.getOrderStatus());

        orderDTOKafkaTemplate.send(ORDER_TOPIC_NAME, savedOrder.getId(), orderDTO);

        return orderDTO;
    }
}
