package com.learn.kafka.client.service.impl;

import com.learn.kafka.entity.Receiver;
import com.learn.kafka.client.repository.ReceiverRepository;
import com.learn.kafka.client.service.ReceiverService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ReceiverServiceImpl implements ReceiverService {

    private final ReceiverRepository receiverRepository;

    @Override
    public Receiver findById(Long id) {
        return receiverRepository.findById(id).orElseThrow(RuntimeException::new);
    }
}
