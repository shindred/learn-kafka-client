package com.learn.kafka.client.service;

import com.learn.kafka.entity.Receiver;

public interface ReceiverService {

    Receiver findById(Long id);
}
