package com.learn.kafka.client.controller;

import com.learn.kafka.entity.Order;
import com.learn.kafka.entity.dto.OrderDTO;
import com.learn.kafka.client.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/order")
@RequiredArgsConstructor
public class OrderController {

    private final OrderService orderService;

    @GetMapping("/{id}")
    public Order getOrderInformation(@PathVariable Long id) {
        return orderService.getOrderInformation(id);
    }

    @PostMapping
    public OrderDTO receive(Order order, Long receiverId) {
        return orderService.save(order, receiverId);
    }
}
